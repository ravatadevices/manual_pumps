#include "RavataPeristalticPump.h"

float RavataPeristalticPump::getCommand()
{
  return command;
}

void RavataPeristalticPump::init(uint8_t pump_addr)
{
  _pumpaddr = pump_addr;
  Wire.begin();
  // pinMode(PRESSURE_PIN, INPUT);
}

void RavataPeristalticPump::setPressure(float newcommand)
{
  //we want to fit the command into a byte, if we need more resolution we can do that later
  command = newcommand;
  float shifted = constrain(command, -1.0, 1.0) * 126; //for int8_t with wiggle room
  Wire.beginTransmission(_pumpaddr);
  Wire.write((int)shifted);
  Wire.endTransmission();
}

float RavataPeristalticPump::readPressure()
{

  // I don't want to send over more than a byte on i2c, so the following is just reading right from the mega
  int unshifted;
  Wire.requestFrom(_pumpaddr,1);
  while (Wire.available())
  {
    unshifted = Wire.read();
  }
  pressure = (float)unshifted / 126.0 - 1.0;

  return pressure;
}
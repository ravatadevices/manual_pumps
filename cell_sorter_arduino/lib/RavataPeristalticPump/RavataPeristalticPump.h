/**
 * \class RavataPeristalticPump 
 * 
 * \author Jason Dekarske (dekarskej@gmail.com)
 * 
 * \brief Communicates with the attiny microcontroller which controls the
 * peristaltic pump. 
 * 
 * \date 2020-03-03
 * 
 * \todo The screen isn't functional on the board because I suspect the voltage regulator isn't correctly implemented.
 * \todo The encoder isn't functional on the board because it is not connected to the atmega on interrupt pins.
 */

#ifndef RavataPeristalticPump_h
#define RavataPeristalticPump_h

#include <Arduino.h>
#include <Wire.h>

#define PUMP_ADDR 0x01

class RavataPeristalticPump
{
  private:
  float pressure;
  float command;
  uint8_t _pumpaddr;

  public:

  /**
   * @brief a quick way to get an estimate of the pressure, use this if you know
   * the pressure controller works well and the tubing is closed!
   *
   * @return float 
   */
  float getCommand();
  
  /**
   * @brief must be called in `setup()`
   * 
   */
  void init(uint8_t pump_addr);

  /**
   * @brief set the command pressure of the system
   * 
   * @param newcommand in psi. will be constrained between +-1psi, which is the limit of the sensor.
   */
  void setPressure(float newcommand);

  /**
   * @return float The system pressure as read from the Attiny's ADC
   */
  float readPressure();

};

#endif